# What does this do ?

This scripts generates the "Weboob" part of the banks.json used by Kresus.

# How it works ?

It scans all the modules from the weboob sources, filters the modules with the capability CapBank, and produces a json on the stdout with the connection fields (plus extra info) for each bank module.

# How to use ?

Clone the weboob repo
Clone this repo.
Go to the root folder of this repo, create a symbolic link to the local weboob repo
`ln -s path/to/the/root/of/local/weboob devel`
run
`python ./bankJsonGenerator.py`
