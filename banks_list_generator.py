#!/usr/bin/python
import argparse
import json
import os
import sys

from weboob.core import WebNip
from weboob.core.modules import ModuleLoadError

# The known modules to be ignored either because they are only called by another module,
# or because they are deprecated.
IGNORE_MODULE_LIST = ['s2e', 'linebourse', 'groupama', 'wellsfargo', 'gmf']

FAKE_MODULES = [
    {
        'backend': 'mock',
        'name': 'Fake Bank 1',
        'uuid': 'fakebank1',
    },
    {
        'backend': 'mock',
        'customFields': [
            {
                'name': 'favorite_code_editor',
                'type': 'select',
                'default': 'sublime',
                'values': [
                    {
                        'label': 'Vim',
                        'value': 'vim'
                    },
                    {
                        'label': 'Emacs',
                        'value': 'emacs'
                    },
                    {
                        'label': 'Sublime Text',
                        'value': 'sublime'
                    }
                ]
            },
            {
                'name': 'secret',
                'type': 'password'
            }
        ],
        'name': 'Fake Bank 2',
        'uuid': 'fakebank2'
    },
    {
        'backend': 'weboob',
        'customFields': [
            {
                'default': 'par',
                'name': 'website',
                'type': 'select',
                'values': [
                    {
                        'value': 'par',
                        'label': 'Particuliers'
                    },
                    {
                        'value': 'pro',
                        'label': 'Professionnels'
                    }
                ]
            },
            {
                'name': 'challengeanswer1',
                'type': 'text'
            }
        ],
        'name': 'Fake Weboob bank',
        'uuid': 'fakeweboobbank'
    }
]

NEEDS_PLACEHOLDER = ['secret', 'birthday']

class ModuleManager(WebNip):
    def __init__(self, modules_path):
        self.modules_path = modules_path
        super(ModuleManager, self).__init__(modules_path=self.modules_path)

    def list_bank_modules(self):
        module_list = []
        for module_name in sorted(self.modules_loader.iter_existing_module_names()):
            try:
                module = self.modules_loader.get_or_load_module(module_name)
            except ModuleLoadError as err:
                print >>sys.stderr, 'Could not import module "%s". Import raised:' % err.module
                print >>sys.stderr, '\t%s' % err
                continue

            if module.has_caps('CapBank') and module_name not in IGNORE_MODULE_LIST:
                module_list.append(module)
        return module_list

    def format_list_modules(self):
        module_list = self.list_bank_modules()

        formatted_list = []
        for module in module_list:
            formatted_module = self.format_kresus(module)

            formatted_list.append(formatted_module)
        return formatted_list

    @staticmethod
    def format_kresus(module):
        '''
        Export the bank module to kresus format
        name : module.description
        uuid: module.name
        backend: "weboob"
        fields: [
            name:
            type:
            ]
        '''
        kresus_module = {}
        kresus_module['name'] = module.description
        kresus_module['uuid'] = module.name
        kresus_module['backend'] = 'weboob'
        fields = []
        config = [item for item in module.config.items() if item[0] not in ['login', 'password', 'username']]

        for key, value in config:
            # Ignore not required fields which are not website
            if not value.required and key != 'website':
                continue

            field = {}
            field['name'] = key

            if value.choices:
                field['type'] = 'select'
                if value.default:
                    field['default'] = value.default
                choices = []
                for k, label in value.choices.iteritems():
                    choices.append(dict(label=label, value=k))
                field['values'] = choices
            else:
                if value.masked:
                    field['type'] = 'password'
                else:
                    field['type'] = 'text'

            if key in NEEDS_PLACEHOLDER:
                field['placeholderKey'] = 'client.settings.%s_placeholder' % key

            fields.append(field)

        if fields:
            kresus_module['customFields'] = fields

        return kresus_module


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates the banks.json for Kresus')
    parser.add_argument('-o', '--output', required=True, help='The file to write the output of the command.')
    parser.add_argument('-w', '--weboob-path', help='The path to the weboob directory', default='../weboob')
    parser.add_argument('-f', '--add-fakemodules', help='Tells whether the fake weboob modules should be added to the list', default=True, action='store_true')
    options = parser.parse_args()

    weboob_path = options.weboob_path
    output_file = options.output
    add_fakemodules = options.add_fakemodules

    modules_path = os.path.join(weboob_path, 'modules')

    if os.path.isdir(modules_path):
        module_manager = ModuleManager(modules_path)
        content = module_manager.format_list_modules()
        if add_fakemodules:
            content += FAKE_MODULES

        try:
            target_file = open(os.path.abspath(output_file), 'w')
            data = json.dumps(content, ensure_ascii=False, indent=4, separators=(',', ': '), sort_keys=True).encode('utf-8')
            target_file.write(data)
            target_file.close()
        except IOError as err:
            print >>sys.stderr, err

        sys.exit(0)

    print >>sys.stderr, 'Unknown weboob directory %s' % weboob_path
    sys.exit(1)
